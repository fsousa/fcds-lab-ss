package problem1;

import java.util.ArrayList;
import java.util.Collections;

public class Bucket {

	private ArrayList<String> bucket;
	private int id;

	public Bucket(int id, ArrayList<String> bucket) {
		this.id = id;
		this.bucket = bucket;
	}

	public int getId() {
		return id;
	}

	public ArrayList<String> getBucket() {
		return this.bucket;
	}

	public void innerSort() {
		Collections.sort(this.bucket);
	}

}
