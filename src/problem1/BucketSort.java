package problem1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class BucketSort {

	public static void main(String[] args) {
	
		int cores = Runtime.getRuntime().availableProcessors();
		run(args[0], cores);
		
	}
	
	public static void run(String fileName, int cores) {
		File file = new File(fileName);
		Scanner sc = null;
		try {
			sc = new Scanner(file);
			int loop = sc.nextInt();
			String[] reads = new String[loop];
			for (int i = 0; i < loop; i++) {
				reads[i] = sc.next();
			}

			long startTime = System.currentTimeMillis();
		
			BucketGroup myBucket = new BucketGroup(reads, cores);
			reads = myBucket.sort();
			myBucket.saveToDisk();
			long stopTime = System.currentTimeMillis();
			long elapsedTime = stopTime - startTime;
			System.out.println(String.format("Time: %d ms. Number of cores: %d",elapsedTime, cores));
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		} finally {
			sc.close();
		}
	}
}
