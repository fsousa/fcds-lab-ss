package problem1;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

public class BucketGroup {

	private String[] array;
	private int cores;
	private List<Thread> myThreads;

	public BucketGroup(String[] array, int cores) {
		this.array = array;
		this.cores = cores;
		this.myThreads = new ArrayList<>();
	}

	public void saveToDisk() {
		FileWriter fw = null;
		try {
			fw = new FileWriter("./bucketsort.out");
			for (int i = 0; i < this.array.length; i++) {
				fw.write(this.array[i] + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public String[] sort() {
		if (this.array.length == 0)
			return this.array; // empty check

		// buckets in maps
		HashMap<Integer, ArrayList<String>> buckets = new HashMap<Integer, ArrayList<String>>();

		// assign array values into buckets
		for (int i = 0; i < array.length; i++) {
			// get first letter of word
			String current = array[i];
			int letterCode = current.charAt(0);

			if (buckets.containsKey(letterCode)) {
				buckets.get(letterCode).add(array[i]);
			} else {
				ArrayList<String> myVector = new ArrayList<String>();
				myVector.add(array[i]);
				buckets.put(letterCode, myVector);
			}
		}
		
		// Sort bucket keys
		Set<Integer> keySet = buckets.keySet();
		Integer[] keys = (Integer[]) keySet.toArray(new Integer[keySet.size()]);
		Arrays.sort(keys);

		ConcurrentLinkedQueue<Integer> queue = new ConcurrentLinkedQueue<>(Arrays.asList(keys));

		for (int i = 0; i < this.cores; i++) {
			Thread t1 = new Thread(new Runnable() {

				@Override
				public void run() {
					Integer ticket = queue.poll();
					while (ticket != null) {
						Bucket myBucket = new Bucket(ticket, buckets.get(ticket));
						myBucket.innerSort();
						ArrayList<String> result = myBucket.getBucket();
						buckets.put(ticket, result);
						ticket = queue.poll();
					}

				}
			}, String.valueOf(i));
			this.myThreads.add(t1);
		}

		for (Thread thread : this.myThreads) {
			thread.start();
		}

		for (Thread thread : this.myThreads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// Place elements into input array
		int index = 0; // keeps global array index
		for (Object key : keys) {
			// retrieve the bucker
			ArrayList<String> bucket = buckets.get(key);

			// pile the current bucket back into array
			for (int j = 0; j < bucket.size(); j++) {
				this.array[index++] = bucket.get(j);
			}
		}

		return this.array;

	}

}
