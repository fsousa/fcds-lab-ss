package experiment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import problem1.BucketSort;
import problem2.MutuallyFriendlyNumbers;
import problem3.Haar;
import problem4.UnboundedKnapsack;
import problem5.W3SAT;

public class Conductor {

	static String outputLogFile = "final-result-time.txt";

	public static void main(String[] args) {

		int[] coresArray = { 1, 2, 4, 8 };
		System.out.println("Running Problem 1");
		for (int i = 0; i < coresArray.length; i++) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			runProblem1(coresArray[i]);
		}
		System.out.println("-------------------");

		System.out.println("Running Problem 2");
		for (int i = 0; i < coresArray.length; i++) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			runProblem2(coresArray[i]);
		}
		System.out.println("-------------------");
		
		System.out.println("Running Problem 3");
		for (int i = 0; i < coresArray.length; i++) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			runProblem3(coresArray[i]);
		}
		System.out.println("-------------------");

		System.out.println("Running Problem 4");
		for (int i = 0; i < coresArray.length; i++) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			runProblem4(coresArray[i]);
		}
		System.out.println("-------------------");

		System.out.println("Running Problem 5");
		for (int i = 0; i < coresArray.length; i++) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			runProblem5(coresArray[i]);
		}
		System.out.println("-------------------");

	}

	private static void runProblem1(int cores) {
		String mediumInput = "bucket.in";

		long[] executions = new long[10];
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			long startTime = System.currentTimeMillis();

			System.out.println(String.format("Bucket - %s - %d", mediumInput, cores));
			BucketSort.run(mediumInput, cores);

			long stopTime = System.currentTimeMillis();
			executions[i] = stopTime - startTime;
		}

		long average = getAverageFromExecutions(executions);
		saveToLog(String.format("Bucket %d %d", cores, average));

	}

	private static void runProblem2(int cores) {
		String largeInput = "friendly.in";

		long[] executions = new long[10];
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			long startTime = System.currentTimeMillis();
			System.out.println(String.format("Friendly - %s - %d", largeInput, cores));

			MutuallyFriendlyNumbers.run(largeInput, cores);

			long stopTime = System.currentTimeMillis();
			executions[i] = stopTime - startTime;
		}

		long average = getAverageFromExecutions(executions);
		saveToLog(String.format("Friendly %d %d", cores, average));
	}
	
	private static void runProblem3(int cores) {

		long[] executions = new long[10];
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			long startTime = System.currentTimeMillis();
			System.out.println(String.format("Haar - image.in - %d", cores));

			Haar.run(cores);

			long stopTime = System.currentTimeMillis();
			executions[i] = stopTime - startTime;
		}

		long average = getAverageFromExecutions(executions);
		saveToLog(String.format("Haar %d %d", cores, average));
	}

	private static void runProblem4(int cores) {
		String largeInput = "knapsack.in";

		long[] executions = new long[10];
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			long startTime = System.currentTimeMillis();
			System.out.println(String.format("Knapsack - %s - %d", largeInput, cores));
			UnboundedKnapsack.run(largeInput, cores);
			long stopTime = System.currentTimeMillis();
			executions[i] = stopTime - startTime;
		}

		long average = getAverageFromExecutions(executions);
		saveToLog(String.format("Knapsack %d %d", cores, average));
	}

	private static void runProblem5(int cores) {
		String mediumInput = "3sat.in";

		long[] executions = new long[10];
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			long startTime = System.currentTimeMillis();
			System.out.println(String.format("3SAT - %s - %d", mediumInput, cores));
			W3SAT.run(mediumInput, cores);
			long stopTime = System.currentTimeMillis();
			executions[i] = stopTime - startTime;
		}

		long average = getAverageFromExecutions(executions);
		saveToLog(String.format("3SAT %d %d", cores, average));
	}

	private static long getAverageFromExecutions(long[] executions) {

		Arrays.sort(executions);
		long sum = 0;
		for (int i = 3; i < 7; i++) {
			sum += executions[i];
		}
		return sum / 4;
	}

	private static void saveToLog(String str) {

		try {

			File file = new File(outputLogFile);

			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fileWritter = new FileWriter(file.getName(), true);
			BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
			bufferWritter.write(str + "\n");
			bufferWritter.close();

			System.out.println("Done");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
