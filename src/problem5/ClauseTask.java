package problem5;

public class ClauseTask {

	private int[][] reads;
	private int[] possibleSolution;

	public ClauseTask(int[][] reads, int[] possibleSolution) {
		this.reads = reads;
		this.possibleSolution = possibleSolution;
	}

	public int[] testClause() {
		for (int j = 0; j < reads.length; j++) {
			boolean result = testSingleClause(reads[j], possibleSolution);
			if (result == true) {
				if (j == reads.length - 1) {
					return possibleSolution;
				}
				continue;
			}
			break;
		}
		return null;
	}

	private boolean testSingleClause(int[] a, int[] b) {

		for (int i = 0; i < a.length; i++) {
			int valueOfA = a[i];
			long clauseValue = 0;
			if (valueOfA < 0) {
				clauseValue = 1 - b[Math.abs(valueOfA) - 1];
			} else {
				clauseValue = b[valueOfA - 1];
			}

			if (clauseValue == 1) {
				return true;
			}
		}

		return false;
	}

}
