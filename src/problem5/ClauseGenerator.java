package problem5;

import java.util.concurrent.atomic.AtomicBoolean;

public class ClauseGenerator implements Runnable {

	private int numberOfVariables;
	public volatile AtomicBoolean terminate;
	public volatile int[] result;
	private long from;
	private long to;
	private int[][] reads;

	public ClauseGenerator(long from, long to, int numberOfVariables, AtomicBoolean terminate, int[][] reads, int[] result) {
		this.numberOfVariables = numberOfVariables;
		this.terminate = terminate;
		this.reads = reads;
		this.from = from;
		this.to = to;
		this.result = result;
	}

	@Override
	public void run() {
		for (long i = from; i < to; i++) {
			if (terminate.get() == true) {
				break;
			}

			int[] output = new int[numberOfVariables];
			char[] representation = Long.toBinaryString(i).toCharArray();
			for (int j = 0; j < output.length; j++) {
				if (j < representation.length ) {
					output[j] = Character.getNumericValue(representation[j]);
				}else {
					output[j] = 0;
				}
			}
			ClauseTask ct = new ClauseTask(reads, output);
			int[] result = ct.testClause();
			if (result != null) {
				copyTo(this.result, result);
				terminate.set(true);
			}
			
		}

	}
	
	private void copyTo(int[] a, int[] b) {
		for (int i = 0; i < b.length; i++) {
			a[i] = b[i];
		}
	}

}
