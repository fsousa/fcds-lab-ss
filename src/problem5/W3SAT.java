package problem5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class W3SAT {

	public static void main(String[] args) {

		int cores = Runtime.getRuntime().availableProcessors();
		run(args[0], cores);

	}

	public static void run(String fileName, int cores) {
		File file = new File(fileName);
		Scanner sc = null;
		try {
			sc = new Scanner(file);
			String[] inputSplited = sc.nextLine().split(" ");
			int loop = Integer.parseInt(inputSplited[0]);
			int power = Integer.parseInt(inputSplited[1]);

			int[][] reads = new int[loop][3];
			for (int i = 0; i < loop; i++) {
				String[] lineSplited = sc.nextLine().split(" ");
				for (int j = 0; j < 3; j++) {
					reads[i][j] = Integer.parseInt(lineSplited[j]);
				}
			}

			long startTime = System.currentTimeMillis();
			
			Executor exec = new Executor(reads, power, cores);
			exec.solve();

			long stopTime = System.currentTimeMillis();
			long elapsedTime = stopTime - startTime;
			System.out.println(String.format("Time: %d ms. Number of cores: %d", elapsedTime, cores));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		} finally {
			sc.close();
		}
	}
}
