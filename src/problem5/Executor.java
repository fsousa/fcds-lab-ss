package problem5;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicBoolean;

public class Executor {

	private List<Thread> myThreads;
	private int cores;
	private int[][] reads;
	private int numberOfVariables;
	public volatile AtomicBoolean terminate = new AtomicBoolean(false);
	public volatile int[] finalResult;
	ArrayList<Long> range;

	public Executor(int[][] reads, int v, int cores) {
		this.cores = cores;
		this.myThreads = new Vector<>();
		this.reads = reads;
		this.numberOfVariables = v;
		this.range = new ArrayList<Long>();
		this.finalResult = this.initializeFinalResultArray(new int[v]);

	}

	public void solve() {
		computeRange();
		checkForPossibilities();
		printResult();

	}

	private void computeRange() {
		long to = (int) Math.pow(2, this.numberOfVariables);
		long factor = to / this.cores + (to % this.cores == 0 ? 0 : 1);
		for (long i = 0; i <= this.cores; i++) {
			range.add(i * factor);
		}
	}

	private void checkForPossibilities() {
		for (int i = 1; i <= cores; i++) {
			Thread t1 = new Thread(new ClauseGenerator(range.get(i - 1), range.get(i), this.numberOfVariables,
					this.terminate, this.reads, this.finalResult), String.valueOf(i));
			this.myThreads.add(t1);
		}

		for (Thread thread : this.myThreads) {
			thread.start();
		}

		for (Thread thread : this.myThreads) {
			try {
				thread.join();
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
	}

	private void printResult() {
		if (finalResult[0] != -1) {
			int sum = 0;
			StringBuilder finalString = new StringBuilder();
			for (int i = finalResult.length - 1; i >= 0; i--) {
				int power = finalResult.length - (i + 1);
				sum += finalResult[i] * Math.pow(2, power);

				finalString.append(finalResult[power]).append(" ");

			}
			System.out.println(String.format("Solution found [%d]: %s", sum, finalString.toString().trim()));
		} else {
			System.out.println("Solution not found.");
		}
	}
	
	private int[] initializeFinalResultArray(int[] r) {
		for (int i = 0; i < r.length; i++) {
			r[i] = -1;
		}
		
		return r;
	}
}
