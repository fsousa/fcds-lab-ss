package problem2;

public class Tuple {

	private Integer value1;
	private Integer value2;

	public Tuple(int value1, int value2) {
		this.value1 = value1;
		this.value2 = value2;
	}

	public int getValue1() {
		return value1;
	}

	public int getValue2() {
		return value2;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return value1 + " - " + value2;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj instanceof Tuple && ((Tuple)obj).getValue1() == this.value1 && ((Tuple)obj).getValue2() == this.value2) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
	@Override
	public int hashCode() {
		return this.value1.hashCode() + this.value2.hashCode();
	}

}
