package problem2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class MutuallyFriendlyNumbers {

	public static void main(String[] args) {
		int cores = Runtime.getRuntime().availableProcessors();
		run(args[0], cores);
	}

	public static void run(String fileName, int cores) {
		Scanner sc = null;
		try {
			File file = new File(fileName);
			try {
				sc = new Scanner(file);
				String[] inputSplited = null;
				ArrayList<Tuple> inputList = new ArrayList<>();
				while (true) {
					inputSplited = sc.nextLine().split(" ");
					Tuple input = new Tuple(Integer.parseInt(inputSplited[0]), Integer.parseInt(inputSplited[1]));

					if (input.getValue1() == 0 && input.getValue2() == 0) {
						break;
					}

					inputList.add(input);
				}

				long startTime = System.currentTimeMillis();

				for (Tuple input : inputList) {
					System.out.println(String.format("Number %d to %d", input.getValue1(), input.getValue2()));
					Executor exec = new Executor(input, cores);
					exec.solve();
				}

				long stopTime = System.currentTimeMillis();
				long elapsedTime = stopTime - startTime;
				System.out.println(String.format("Time: %d ms.", elapsedTime));

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} finally {
			sc.close();
		}
	}

}
