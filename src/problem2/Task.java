package problem2;

import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

public class Task {

	private int number;
	private int position;
	ConcurrentHashMap<Tuple, Vector<Integer>> resultTable;

	public Task(int number, int position, ConcurrentHashMap<Tuple, Vector<Integer>> resultTable) {
		this.setNumber(number);
		this.setPosition(position);
		this.resultTable = resultTable;

	}

	public void run() {

		int sum = factorsSum(getNumber());
		int gcd = gcd(sum, getNumber());

		Tuple key = new Tuple(sum / gcd, getNumber() / gcd);

		if (!resultTable.containsKey(key)) {
			Vector<Integer> positionArray = new Vector<>();
			positionArray.add(position);
			resultTable.put(key, positionArray);
		} else {
			resultTable.get(key).add(position);
		}

	}

	private int gcd(int a, int b) {
		if (a == 0)
			return b;

		while (b != 0) {
			if (a > b)
				a = a - b;
			else
				b = b - a;
		}

		return a;
	}

	private int factorsSum(int num) {
		int incrementer = 1;
		int sum = 0;
		if (num % 2 != 0) {
			incrementer = 2; // only test the odd ones
		}

		for (int i = 1; i <= num / 2; i = i + incrementer) {
			if (num % i == 0) {
				sum += i;
			}
		}
		sum += num;

		return sum;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

}
