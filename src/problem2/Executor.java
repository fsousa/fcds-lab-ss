package problem2;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Executor {

	private List<Thread> myThreads;
	private Tuple inputPair;
	private int cores;
	private ConcurrentLinkedQueue<Tuple> queue;
	private Vector<Integer> groupNumber;

	public ConcurrentHashMap<Tuple, Vector<Integer>> resultTable;

	public Executor(Tuple inputPair, int cores) {
		this.setInputPair(inputPair);
		this.setCores(cores);
		this.queue = new ConcurrentLinkedQueue<>();
		this.groupNumber = new Vector<>(inputPair.getValue2() - inputPair.getValue1());
		this.myThreads = new Vector<>();
		this.resultTable = new ConcurrentHashMap<Tuple, Vector<Integer>>();
	}

	public void solve() {

		initialization();
		createAndExecuteTasks();
		printPairResults();

	}

	public int getCores() {
		return cores;
	}

	public void setCores(int cores) {
		this.cores = cores;
	}

	public Tuple getInputPair() {
		return inputPair;
	}

	public void setInputPair(Tuple inputPair) {
		this.inputPair = inputPair;
	}

	private void initialization() {
		int pos = 0;
		for (int i = getInputPair().getValue1(); i <= getInputPair().getValue2(); i++) {
			groupNumber.add(i);
			queue.add(new Tuple(i, pos));
			pos++;
		}
	}

	private void createAndExecuteTasks() {
		for (int i = 0; i < this.cores; i++) {
			Thread t1 = new Thread(new Runnable() {

				@Override
				public void run() {
					Tuple ticket = queue.poll();
					while (ticket != null) {
						Task myTask = new Task(ticket.getValue1(), ticket.getValue2(), resultTable);
						myTask.run();
						ticket = queue.poll();
					}

				}
			}, String.valueOf(i));
			this.myThreads.add(t1);
		}

		for (Thread thread : this.myThreads) {
			thread.start();
		}

		for (Thread thread : this.myThreads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void printPairResults() {
		for (Tuple key : resultTable.keySet()) {
			Vector<Integer> values = resultTable.get(key);
			if (values.size() > 1) {
				for (int i = 0; i < values.size(); i++) {
					for (int j = i+1; j < values.size(); j++) {
						System.out.println(String.format("%d and %d are FRIENDLY", groupNumber.get(values.get(i)),
								groupNumber.get(values.get(j))));
					}
					
				}
				

			}
		}
	}
}
