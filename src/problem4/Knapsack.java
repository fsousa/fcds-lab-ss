package problem4;

import java.util.ArrayList;
import java.util.List;

/*
 * Disclamer: Main code from https://github.com/zhezha/Unbounded_Knapsack_Problem
 * 
 */

public class Knapsack {

	public volatile int totalWeight;
	public volatile int[] weights;
	public volatile int[] values;
	public volatile int[] c;
	public volatile int[] p;
	private List<Thread> myThreads;
	private int cores;

	public Knapsack(int totalWeight, int[] itemListV, int[] itemListW, int cores) {
		this.totalWeight = totalWeight;
		this.weights = itemListW;
		this.values = itemListV;
		this.myThreads = new ArrayList<>();
		this.cores = cores;
	}

	public int[] findOptimalSolution() {

		// weights and totalWeight are devided by their gcd to reduce the size of the bag
		int gcdWeights = ngcd(weights, weights.length);

		for (int j = 0; j < weights.length; j++) {
			weights[j] /= gcdWeights;
		}

		totalWeight /= gcdWeights;

		return solveDP(values, weights, totalWeight);

	}

	// solving with dynamic programming
	int[] solveDP(int[] values, int[] weights, int totalWeight) {

		int n = values.length;
		
		c = new int[totalWeight + 1];
		p = new int[totalWeight + 1];

		int[] range = computeRange(values, cores);
		for (int i = 1; i <= cores; i++) {
			Thread t1 = new Thread(new Solver(range[i - 1], range[i]));
			myThreads.add(t1);
		}

		for (Thread thread : this.myThreads) {
			thread.start();
		}

		for (Thread thread : this.myThreads) {
			try {
				thread.join();
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
		
		int[] bestAmountEachItem = new int[n];
		int minWeights = getMin(weights);
		
		int j = totalWeight;
		while (j >= minWeights) {
			bestAmountEachItem[p[j]]++;
			j -= weights[p[j]];
		}

		return bestAmountEachItem;
	}

	// Get the gcd of the first n elements in given array
	private int ngcd(int[] array, int n) {
		int result = array[0];
		for (int i = 1; i < array.length; i++) {
			result = gcd(result, array[i]);
		}
		return result;
	}

	private int gcd(int a, int b) {
		if (a == 0)
			return b;

		while (b != 0) {
			if (a > b)
				a = a - b;
			else
				b = b - a;
		}

		return a;
	}

	// Get the minimum value of given array.
	private int getMin(int[] array) {
		int result = Integer.MAX_VALUE;
		for (int i = 0; i < array.length; i++) {
			if (result > array[i]) {
				result = array[i];
			}
		}
		return result;
	}

	private int[] computeRange(int[] array, int cores) {
		int to = array.length;
		int factor = to / cores + (to % cores == 0 ? 0 : 1);
		int[] range = new int[cores + 1];
		for (int i = 0; i <= cores; i++) {
			range[i] = (i * factor);
		}
		return range;
	}

	class Solver implements Runnable {

		private int from;
		private int to;

		public Solver(int from, int to) {
			this.from = from;
			this.to = to;
		}

		@Override
		public void run() {
			for (int i = from; i < to; i++) {
				
				for (int j = weights[i]; j <= totalWeight; j++) {
					if (c[j - weights[i]] + values[i] > c[j]) {
						c[j] = c[j - weights[i]] + values[i];
						p[j] = i;
					}
				}
			}
		}
	}
}