package problem4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class UnboundedKnapsack {

	public static void main(String[] args) {
		int cores = Runtime.getRuntime().availableProcessors(); 
		run(args[0], cores);
	}
	
	public static void run(String fileName, int cores) {
		long startTime = System.currentTimeMillis();
		Scanner sc = null;
		try {
			File file = new File(fileName);
			try {
				sc = new Scanner(file);
				String[] inputSplited = sc.nextLine().split(" ");
				int loop = Integer.parseInt(inputSplited[0]);
				int maxWeight = Integer.parseInt(inputSplited[1]);
				int[] itemsListV = new int[loop];
				int[] itemsListW = new int[loop];

				for (int i = 0; i < loop; i++) {
					String[] splitted = sc.nextLine().split(" ");
					int v = Integer.valueOf(splitted[0]);
					int w = Integer.valueOf(splitted[1]);
					itemsListV[i] = v;
					itemsListW[i] = w;
				}

				
				Knapsack ks = new Knapsack(maxWeight, itemsListV, itemsListW, cores);
				int[] result = ks.findOptimalSolution();
				
				//Print Result
				int total = 0;
				for (int i = 0; i < result.length; i++) {
					total += result[i] * itemsListV[i];
				}
				System.out.println(total);

				long stopTime = System.currentTimeMillis();
				long elapsedTime = stopTime - startTime;
				System.out.println(String.format("Time: %d ms.", elapsedTime));

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} finally {
			sc.close();
		}

	}
}
