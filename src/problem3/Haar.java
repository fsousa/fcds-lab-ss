package problem3;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


//Code from https://github.com/anthonyray/HaarWavelets
public class Haar {

	private static List<List<Integer>> reads;

	public static void main(String[] args) {

		int cores = Runtime.getRuntime().availableProcessors();
		run(cores);

	}

	public static void run(int cores) {

		long total = 0;
		long startTime = System.currentTimeMillis();
		read();		
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		total += elapsedTime;
		System.out.println(String.format("Read: %d ms.", elapsedTime));
		startTime = System.currentTimeMillis();
		HaarWaveletMatrix twoD = new HaarWaveletMatrix(reads, cores);
		List<List<Integer>> result = twoD.standardDecomposition();
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		total += elapsedTime;
		System.out.println(String.format("Calculation: %d ms.", elapsedTime));
		startTime = System.currentTimeMillis();
		saveMatrix(result);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		total += elapsedTime;
		System.out.println(String.format("Write: %d ms.", elapsedTime));
		System.out.println(String.format("Total: %d ms.", total));
		//compareResults();
	}
	
//	private static void read() {
//		int[][] data2D = {{9, 7, 3, 5 },{2, 4, 8, 4 },{1, 7, 13, 4 },{19, 27, 53, 45 }};
//		reads = new ArrayList<List<Integer>>();
//		for (int i = 0; i < 4; i++) {
//			List<Integer> inner = new ArrayList<>();
//			for (int j = 0; j < 4; j++) {
//				inner.add(data2D[i][j]);
//			}
//			reads.add(inner);
//		}
//	}

	private static void read() {
		DataInputStream dataIn;
		try {
			dataIn = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("image.in"))));
			int numberOfColumns = dataIn.readInt();// read 4 bytes
			reads = new ArrayList<List<Integer>>();
			for (int i = 0; i < numberOfColumns; i++) {
				List<Integer> inner = new ArrayList<>();
				for (int j = 0; j < numberOfColumns; j++) {
					inner.add(dataIn.readInt());
				}
				reads.add(inner);
			}

			dataIn.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static void saveMatrix(List<List<Integer>> output) {
		DataOutputStream dataOut;
		try {
			dataOut = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(new File("image.out"))));
			dataOut.writeInt(output.size());
			for (List<Integer> list : output) {
				for (Integer integer : list) {
					dataOut.writeInt(integer);
				}
			}
			dataOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

//	private static void compareResults() {
//		DataInputStream dataIn = null;
//		DataInputStream dataIn2 = null;
//		try {
//			dataIn = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("image.out"))));
//			dataIn2 = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("image.out.correct"))));
//			int numberOfColumns = dataIn.readInt();// read 4 bytes
//			int numberOfColumns2 = dataIn2.readInt();// read 4 bytes
//
//			if (numberOfColumns != numberOfColumns2) {
//				System.out.println("False - Size doesn't match");
//				System.exit(1);
//			}
//
//			while (true) {
//				try {
//					int a = dataIn.readInt();
//					int b = dataIn2.readInt();
//					if (a != b) {
//						System.out.println("False - Wrong calculation");
//						System.exit(1);
//					}
//				} catch (java.io.EOFException eof) {
//					break;
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//			dataIn.close();
//			dataIn2.close();
//		} catch (
//
//		IOException e) {
//			e.printStackTrace();
//		}
//
//		System.out.println("True");
//	}

}
