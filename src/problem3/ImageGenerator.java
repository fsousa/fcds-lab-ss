package problem3;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ImageGenerator {
	
	public static void main(String[] args) {
		
		int size = 1024 * 6;
		List<Integer> list = new ArrayList<Integer>(size * size);
		Random rand = new Random(System.currentTimeMillis());
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				list.add(rand.nextInt(10000000));
			}
		}
		
		saveList(list, size);
		
	}
	
	private static void saveList(List<Integer> output, int size) {
		DataOutputStream dataOut;
		try {
			dataOut = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(new File("image.in"))));
			dataOut.writeInt(size);
			for (Integer integer : output) {
				dataOut.writeInt(integer);
			}
			dataOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
