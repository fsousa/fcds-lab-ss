package problem3;

import java.util.ArrayList;
import java.util.List;

public class HaarWaveletMatrix {
	private List<List<Integer>> matrix;
	private List<List<Integer>> tempMatrix;
	private List<Thread> myThreads;
	private int cores;

	public HaarWaveletMatrix(List<List<Integer>> data, int cores) {
		this.matrix = data;
		this.cores = cores;
		this.myThreads = new ArrayList<>();
	}

	public List<List<Integer>> standardDecomposition() {

		processRows();
		concurrentTranspose();
		processRows();
		concurrentTranspose();
		
		return matrix;
	}
	
	public void concurrentTranspose() {
		tempMatrix = new ArrayList<List<Integer>>(matrix.size());
		for (List<Integer> list : matrix) {
			tempMatrix.add(new ArrayList<Integer>(list));
		}

		int[] range = computeRange(matrix.size(), this.cores);
		for (int i = 1; i <= cores; i++) {
			Thread t1 = new Thread(new ParallelTranspose(range[i - 1], range[i]));
			myThreads.add(t1);
		}

		for (Thread thread : this.myThreads) {
			thread.start();
		}

		for (Thread thread : this.myThreads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		matrix.clear();
		matrix.addAll(tempMatrix);
		myThreads.clear();
	}

	public void processRows() {
		int size = matrix.size();
		int[] range = computeRange(size, cores);
		for (int i = 1; i <= cores; i++) {
			Thread t1 = new Thread(new SolverRows(range[i - 1], range[i]));
			myThreads.add(t1);
		}

		for (Thread thread : this.myThreads) {
			thread.start();
		}

		for (Thread thread : this.myThreads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		myThreads.clear();
	}

	public List<Integer> decompositionRow(List<Integer> row) {
		int steps = row.size();
		while (steps > 1) {
			row = decompositionStep(steps, row);
			steps = steps / 2;
		}
		return row;
	}

	public List<Integer> decompositionStep(int step, List<Integer> row) {
		List<Integer> newRow = new ArrayList<>(row);
		for (int i = 0; i < step / 2; i++) {

			int a = (row.get(2 * i) + row.get(2 * i + 1)) / 2;
			int b = (row.get(2 * i) - row.get(2 * i + 1)) / 2;

			newRow.set(i, a);
			newRow.set(step / 2 + i, b);
		}

		return newRow;

	}

	public List<List<Integer>> getMatrix() {
		return matrix;
	}

	private int[] computeRange(int size, int cores) {
		int to = size;
		int factor = to / cores + (to % cores == 0 ? 0 : 1);
		int[] range = new int[cores + 1];
		for (int i = 0; i <= cores; i++) {
			range[i] = (i * factor);
		}
		return range;
	}

	private class ParallelTranspose implements Runnable {

		private int from;
		private int to;

		public ParallelTranspose(int from, int to) {
			this.from = from;
			this.to = to;
		}

		@Override
		public void run() {

			for (int i = from; i < to; i++) {
				for (int j = 0; j < matrix.size(); j++) {
					Integer origin = matrix.get(j).get(i);
					tempMatrix.get(i).set(j, origin);

				}
			}
		}

	}

	private class SolverRows implements Runnable {

		private int from;
		private int to;

		public SolverRows(int from, int to) {
			this.from = from;
			this.to = to;
		}

		@Override
		public void run() {
			for (int i = from; i < to; i++) {
				matrix.set(i, decompositionRow(matrix.get(i)));
			}
		}

	}
}
