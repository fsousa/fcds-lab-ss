package problem3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HaarWaveletSolver {

	List<Integer> matrix;
	int size;
	
	public HaarWaveletSolver(List<Integer> data){
		matrix = data;
		size = matrix.size();
	}

	public void show(){
		for (int i = 0; i < size; i++) {
            System.out.print(matrix.get(i) + ", ");
        }
        System.out.println("");
	}
	
	public List<Integer> standardDecomposition(){
		int steps = size;
		List<Integer> row = matrix;
		while (steps > 1){
			row = decompositionStep(steps, row);
			steps = steps / 2;
		}
		
		matrix = row;
		
		return matrix;
	}
	
	public List<Integer> decompositionStep(int step,List<Integer> row){
		List<Integer> newRow = new ArrayList<>(row);
		System.out.println(Arrays.toString(row.toArray()));
		for (int i=0 ; i < step/2 ; i++){
			
			int a = (row.get(2*i) + row.get(2*i + 1))/2;
			int b = (row.get(2*i) - row.get(2*i + 1))/2;
			
			newRow.set(i, a);
			newRow.set(step/2 + i, b);
		}
		
		System.out.println(Arrays.toString(newRow.toArray()));
		return newRow;
		
	}
}
